<?php

namespace Creativehandles\ChGallery\Plugins\Gallery;

use App\Helpers\Slug;
use Creativehandles\ChGallery\Plugins\Gallery\Models\Image;
use App\Helpers\Plugins;
use Creativehandles\ChGallery\Plugins\Plugin;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Artisan;


class Gallery extends Plugin {

  public $imagesPath;
  public $thumbnailsPath;
  public $thumbnailsPath200;
  public $thumbnailsPath400;
  public $thumbnailsPath600;
  public $thumbnailsPath800;
  public $thumbnailsPath1000;

  public function __construct()
  {
    $this->imagesPath = storage_path('app/public').'/images'; // path for original images
    $this->thumbnailsPath = storage_path('app/public').'/thumbnails';
    $this->thumbnailsPath200 = storage_path('app/public').'/thumbnails/200';
    $this->thumbnailsPath400 = storage_path('app/public').'/thumbnails/400';
    $this->thumbnailsPath600 = storage_path('app/public').'/thumbnails/600';
    $this->thumbnailsPath800 = storage_path('app/public').'/thumbnails/800';
    $this->thumbnailsPath1000 = storage_path('app/public').'/thumbnails/1000';
  }

  /**
   * This method is run by installator.
   * Here you should insert initial installation commands
   */
  public function install()
  {
    $called = Artisan::call('storage:link');

    if(!file_exists($this->imagesPath)) {
      mkdir($this->imagesPath);
    }

    if(!file_exists($this->thumbnailsPath)) {
      mkdir($this->thumbnailsPath);
    }

    if(!file_exists($this->thumbnailsPath200)) {
      mkdir($this->thumbnailsPath200);
    }

    if(!file_exists($this->thumbnailsPath400)) {
      mkdir($this->thumbnailsPath400);
    }

    if(!file_exists($this->thumbnailsPath600)) {
      mkdir($this->thumbnailsPath600);
    }

    if(!file_exists($this->thumbnailsPath800)) {
      mkdir($this->thumbnailsPath800);
    }

    if(!file_exists($this->thumbnailsPath1000)) {
      mkdir($this->thumbnailsPath1000);
    }
  }

  /**
   * This method is run by installator.
   * Here you should insert initial installation commands
   */
  public function uninstall()
  {

    Storage::deleteDirectory('/public/images');
    Storage::deleteDirectory('/public/thumbnails');
  }

  /**
   * Clears file name from trash.
   *
   * @param String $filename
   * @return string
   */
  public function clearFileName(String $filename) : string
  {
    $filename = str_replace('public/images/', '', $filename);
    $filename = str_replace('storage/images/', '', $filename);

    $split = explode(".", $filename);

    return $split[0];
  }

  /**
   * Returns extension of filename
   *
   * @param Strin $filename
   * @return string
   */
  public function getExt(String $filename) : string
  {
    $split = explode(".", $filename);

    return end($split);
  }

  /**
   * Return path of thumbnail
   *
   * @param $filename
   * @param Int $size
   *
   * @return String
   */
  public static function thumbnail($filename, Int $size) : string
  {
    if(!is_null($filename)) {
      $extension = (new self)->getExt($filename);
      $filename = (new self)->clearFileName($filename);

      $thumbnailPath = Storage::url('public/thumbnails/');

//      return $thumbnailPath.$size.'/'.$filename.'.'.$extension;

        $path =$thumbnailPath.$size.'/'.$filename.'.'.$extension;

        if(env('GALLERY_PATH',false)){
            return str_replace('/storage/','/backend/public/storage/',$path);
        }

	return $path;
    }

    return false;
  }


    public function getImagesLike($keyword=null)
    {
//        $images = Image::where('title','LIKE','%'.$keyword.'%')->orWhere('alt','LIKE','%'.$keyword.'%')->pluck('image_path')->all();

        $images = Image::join("image_translations","images.id","=","image_translations.image_id")
            ->where('image_translations.title','LIKE','%'.$keyword.'%')
            ->orWhere('image_translations.alt','LIKE','%'.$keyword.'%')
            ->pluck('image_path')->all();

        if(!$images){
            return [];
        }

        return self::getImageWithData($images);
    }

  public static function getImageWithData($selectedImages = [], $sort = true, $fillEmptyArray = true){
      $empty = (empty($selectedImages)) ? true : false;
      $selectedImages = ($empty === false) ? $selectedImages : Storage::files('public/images');
      $reParsedImages = [];

      if ($fillEmptyArray === false && $empty === true) {
          return $selectedImages = [];
      }

      $galleryClass = new Gallery();
      foreach($selectedImages as $image){
          $imageModel = new Image();
          $imagePath = ($empty !== true) ? str_replace("/storage", "public", $image) : $image;
          $imageData = $imageModel->getImageData($imagePath);
          if ($imageData) {
              $reParsedImages[] = (object) ["id" => $imageData->id, "path" => $image, 'locale' => $imageData->locale, "title" => (($imageData->title !== null) ? $imageData->title : $galleryClass->clearFileName($image)), "alt" => $imageData->alt];
          }
      }

      if($sort === true){
          usort($reParsedImages, function ($a, $b) {
              return $a->title > $b->title;
          });
      }

      return $reParsedImages;
  }

  /**
   * Returns a component to choose an image in instance
   *
   * @param $image
   * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
   */
  public static function RenderChooseButton(String $image = null)
  {
    $images = self::getImageWithData();

    return view('Admin.Gallery.Choose')
      ->with('images', $images)
      ->with('image', $image);
  }

    public static function RenderMultipleImageSelector($selectedImages=[],$customName="multiImageList",$multiplePick=true)
    {

        $selectedImages = self::getImageWithData($selectedImages, false, false);
        $images = self::getImageWithData();
        $ids = array_column((array) $selectedImages, 'id');

        return view('Admin.Gallery.ChooseMultiple')
            ->with('images', $images)
            ->with('multiplePick',$multiplePick)
            ->with('customName',$customName)
            ->with('selectedImages', $selectedImages)
            ->with('selectedIds', $ids);
    }


    public static function renderSingleIimagePicker($selectedImage = null){
        $selectedImage = self::getImageWithData([$selectedImage], false, false);
        $images = self::getImageWithData();
        $ids = array_column((array) $selectedImage, 'id');

        return view('Admin.Gallery.ChooseSingle')
        ->with('images', $images)
        ->with('selectedImage', $selectedImage)
        ->with('selectedIds', $ids);
    }
}
