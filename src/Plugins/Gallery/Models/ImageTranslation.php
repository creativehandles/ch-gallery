<?php

namespace Creativehandles\ChGallery\Plugins\Gallery\Models;

use Illuminate\Database\Eloquent\Model;

class ImageTranslation extends Model
{
    protected $table = 'image_translations';
    public $fillable = ['title', 'alt'];
}
