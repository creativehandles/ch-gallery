<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <div class="row">
                <div class="col-md-12">
                    <a href="javascript:void(0);" data-toggle="modal" data-target="#SingleChooseGallery"
                        class="btn btn-secondary btn-block-sm">{{ __('general.Choose Images') }}</a>
                </div>
            </div>
            <div class="row mt-5" id="singleImagePreviewList">
                @if (isset($selectedImage))
                    @foreach ($selectedImage as $image)
                        <div class="col-md-2">
                            <img class="img-thumbnail img-fluid" src="{{ $image->path }}" itemprop="thumbnail"
                                height="100%">
                        </div>
                    @endforeach
                @endif
            </div>

            <div class="modal fade text-left" id="SingleChooseGallery" tabindex="-1" role="dialog"
                aria-labelledby="myModalLabel16" style="display: none;" aria-hidden="true">
                <div class="modal-dialog modal-xl" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel16">{{ __('general.Choose Images') }} single</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row masonry-grid my-gallery single-picker">
                                <div class="col-md-12">
                                    @if (count($images))
                                        <select name="{{ !empty($customName) ? $customName : 'image_url' }}"
                                            class="single-image-picker">
                                            @foreach ($images as $key => $image)
                                                <option value=""></option>
                                                <option {{ in_array($image->id, $selectedIds) ? 'selected' : '' }}
                                                    data-img-src="{{ Gallery::thumbnail($image->path, 200) }}"
                                                    data-img-title="{{ $image->path }}"
                                                    value="{{ Storage::url($image->path) }}">{{ $image->path }}
                                                </option>
                                            @endforeach
                                        </select>
                                    @else
                                        <div class="col-lg-3 col-md-12">
                                            <div href="{{ route('admin.CreateFolder') }}" class="card blocks-folder">
                                                <div class="card-content">
                                                    <div class="card-body text-center">
                                                        <h4 class="card-title">@lang('gallery.noImages')</h4>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary"
                                data-dismiss="modal">{{ __('general.Done') }}</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@push('component-scripts')
    <script>
        $(function() {
            if ($(".single-image-picker").length > 0) {
                $(".single-image-picker").imagepicker();
                $(".single-image-picker").data('picker').sync_picker_with_select();
            }
        });

        // single-image-picker

        $('body').on('click', '.single-picker .thumbnails .thumbnail', function() {
            //get all selected ones
            var isSelected = $(this).hasClass("selected");
            var itemPath = $(this).find("img").attr("src");
            var itemOption = $(".single-image-picker option[data-img-src='" + itemPath + "']");
            var itemSrc = $(itemOption).val();

            if (isSelected) {
                $('#singleImagePreviewList').html(
                    '<div class="col-md-2"><img class="img-thumbnail img-fluid" src="' + itemSrc +
                    '" itemprop="thumbnail" height="100%"></div>');
            } else {
                $('#singleImagePreviewList').find(".img-thumbnail[src='" + itemSrc + "']").closest("div").remove();
            }

            // var options = $(".single-image-picker option");
            // var optionsCount = $(options).length;
            // $(itemOption).insertAfter(options[optionsCount-1]);

        });
    </script>
@endpush
