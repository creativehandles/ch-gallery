@extends('Admin.layout')

@section('styles')
  <link rel="stylesheet" type="text/css" href="{{ asset("vendors/js/gallery/photo-swipe/photoswipe.css") }}">
  <link rel="stylesheet" type="text/css" href="{{ asset("vendors/js/gallery/photo-swipe/default-skin/default-skin.css") }}">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6/css/select2.min.css" rel="stylesheet"/>
@endsection

@section('content')
  <div class="content-header row">
    <div class="content-header-left col-md-6 col-12 mb-2">
      <h3 class="content-header-title mb-0">@lang('gallery.title')</h3>
      <div class="row breadcrumbs-top">
        <div class="breadcrumb-wrapper col-12">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('admin.gallery') }}">@lang('content.pageNames.dashboard')</a>
            </li>
            <li class="breadcrumb-item active">@lang('gallery.title')
            </li>
          </ol>
        </div>
      </div>
    </div>
    <div class="content-header-right col-md-6 col-12 mb-2">
      <div class="btn-group mb-1 pull-right">
        <a href="javascript:void(0);" data-toggle="modal" data-target="#xlarge" class="btn btn-secondary btn-block-sm"><i class="ft-upload"></i> @lang('gallery.uploadFile')</a>
        &nbsp;
        <a href="#" id="saveImagesChanges" class="btn btn-secondary btn-block-sm disabled"><i class="fa fa-check-square-o"></i> @lang('gallery.saveImages')</a>
      </div>
    </div>
  </div>
  <div class="content-body">
    @if(session('created') === true)
      <div class="alert bg-success alert-dismissible mb-2" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
        <strong>@lang('blocks.folderCreatedGrats')</strong> @lang('blocks.folderCreatedRest')
      </div>
    @endif

    @if(session('created') === false)
      <div class="alert bg-danger alert-dismissible mb-2" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
        <strong>@lang('blocks.folderNotCreatedBold')</strong> @lang('blocks.folderNotCreatedRest')
      </div>
    @endif


    @if(session('success') === true)
      <div class="alert bg-success alert-dismissible mb-2" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
        <strong>@lang('blocks.folderRenamedBold')</strong>
      </div>
    @endif

    @if(session('status') === true)
      <div class="alert bg-success alert-dismissible mb-2" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
        <strong>@lang('blocks.folderRestored')</strong>
      </div>
    @endif

    @if(request()->has('deleted'))
      <div class="alert bg-info alert-dismissible mb-2" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
        <strong>@lang('blocks.deletedFoldersShowedBold')</strong> @lang('blocks.deletedFoldersShowedRest')
      </div>
    @endif

    <div class="modal fade text-left" id="xlarge" tabindex="-1" role="dialog" aria-labelledby="myModalLabel16"
         style="display: none;" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel16">@lang('gallery.modal.heading')</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">
            <input type="file" class="filepond" name="filepond[]"/>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn grey btn-outline-primary"  data-dismiss="modal">@lang('gallery.modal.done')</button>
            <button type="button" class="btn grey btn-outline-secondary" id="hideUploading" data-dismiss="modal">@lang('gallery.modal.close')</button>
          </div>
        </div>
      </div>
    </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <form action="" id="seachArticleForm">
                                <fieldset>
                                    <div class="input-group">
                                        <input type="text" id="search-images" class="form-control" placeholder="{{__('gallery.Search by title or alt')}}" aria-describedby="button-addon2" name="keyword" value="{{ isset($keyword) ? $keyword : '' }}">
                                        <div class="input-group-append" id="button-addon2">
                                            <button class="btn btn-primary" type="submit"><i class="fa fa-search"></i></button>
                                        </div>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>

      <div class="row masonry-grid my-gallery"style="margin-left: 0px !important;margin-right: 0px !important;" id="figure-container">
        @if(count($images))
          @foreach($images as $key=>$image)
              <figure class="grid-item" itemprop="associatedMedia" itemscope="" itemtype="http://schema.org/ImageObject">
                  <a href="#" data-id="{{$image->id}}" class="delete-image" style="text-align: left; left: 6px; top: 5px; position: absolute;color: #404e67;">
                      <i class="fa fa-close"></i>
                  </a>
                <img class="img-thumbnail img-fluid" src="{{ Gallery::thumbnail($image->path, 200) }}" itemprop="thumbnail" height="100%">
                <a href="{{ Gallery::thumbnail($image->path, 800) }}" target="_blank" class="external-open">
                  <i class="fa fa-external-link-square"></i>
                </a>
                <span class="editable">
                  <a class="editable-wrap">
                    <span>
                        <b>@lang('gallery.Locale')</b>
                        </br>
                        <select name="form_locale[{{$image->id}}]" data-imageId="{{$image->id}}" class="custom-select h-100" style="width: 88%;">
                            @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $locale)
                                <option value="{{ $localeCode }}" {{ ($image->locale === $localeCode) ? 'selected' : '' }}>{{ $locale['native'] }}</option>
                            @endforeach
                        </select>
                    </span>
                    <span><b>@lang('gallery.imageTitle')</b></br><input type="text" name="imageTitle[{{$image->id}}]" value="{{$image->title}}" /></span>
                    <span><b>@lang('gallery.imageAlt')</b></br><input type="text" name="imageAlt[{{$image->id}}]" value="{{$image->alt}}" /></span>
                      <br>
                      <span><b>{{__('gallery.Copy to clipboard')}}</b></span>
                      <a href="#" class="gallery-clipboard" data-clipboard-text="{{URL::to('/')}}{{Gallery::thumbnail($image->path,200)}}">200 </a>
                      <a href="#" class="gallery-clipboard" data-clipboard-text="{{URL::to('/')}}{{Gallery::thumbnail($image->path,400)}}">400 </a>
                      <a href="#" class="gallery-clipboard" data-clipboard-text="{{URL::to('/')}}{{Gallery::thumbnail($image->path,600)}}">600 </a>
                      <a href="#" class="gallery-clipboard" data-clipboard-text="{{URL::to('/')}}{{Gallery::thumbnail($image->path,800)}}">800 </a>
                      <a href="#" class="gallery-clipboard" data-clipboard-text="{{URL::to('/')}}{{Gallery::thumbnail($image->path,1000)}}">1000 </a>
                      <a href="#" class="gallery-clipboard" data-clipboard-text="{{URL::to('/')}}{{'/storage/'.str_replace('public/','',$image->path)}}">ORIGINAL </a>
                  </a>
                </span>
              </figure>
          @endforeach
        @else
          <div class="col-lg-3 col-md-12">
            <div href="#" class="card blocks-folder">
              <div class="card-content">
                <div class="card-body text-center">
                  <h4 class="card-title">@lang('gallery.noImages')</h4>
                </div>
              </div>
            </div>
          </div>
        @endif
      </div>
  </div>
@endsection

@section("scripts")
  <script src="{{ asset("vendors/js/extensions/sweetalert.min.js") }}" type="text/javascript"></script>
  <script src="{{ asset("js/scripts/filepond/filepond.min.js") }}" type="text/javascript"></script>
  <script src="{{ asset("js/scripts/filepond/filepond-plugin-file-validate-size.min.js") }}" type="text/javascript"></script>
  <script src="{{ asset("js/scripts/filepond/filepond-plugin-file-validate-type.min.js") }}" type="text/javascript"></script>
  <script src="{{ asset("js/scripts/filepond/filepond-plugin-image-preview.min.js") }}" type="text/javascript"></script>
  <script src="{{ asset("js/scripts/filepond/filepond.jquery.js") }}" type="text/javascript"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.4/clipboard.min.js"></script>
  <script>
    $(document).ready(function () {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

      $.fn.filepond.registerPlugin(FilePondPluginFileValidateSize);
      $.fn.filepond.registerPlugin(FilePondPluginFileValidateType);
      $.fn.filepond.registerPlugin(FilePondPluginImagePreview);

      $.fn.filepond.setDefaults({
        maxFileSize: '5MB',
        labelMaxFileSizeExceeded: 'Soubor je příliž velký',
        labelMaxFileSize: 'Maximální velikost souboru je {filesize}',
        acceptedFileTypes: ['image/*'],
        labelFileTypeNotAllowed: 'Nepodporovaný formát souboru',
        server: {
          url: '{{ route('admin.uploadImage') }}',
        }
      });

      $('.filepond').filepond();
      $('.filepond').filepond('allowMultiple', true);


      $('#search-images').on('keyup',function(){

        var baseUrl = "{{URL::to('/')}}";


          $.ajax({
              data: {keyword:$(this).val()},
              method: 'GET',
              url: "{{ route("admin.ajaxSearchImage") }}",
          }).done(function (response) {

              $('#figure-container').html('');
              var copyToText = '{{__('gallery.Copy to clipboard')}}';
              $.each(response,function (k,v) {
                  var imgPath = v.path;
                  var t200 = imgPath.replace('public/images/','/storage/thumbnails/200/');
                  var t400 = imgPath.replace('public/images/','/storage/thumbnails/400/');
                  var t800 = imgPath.replace('public/images/','/storage/thumbnails/800/');
                  var t600 = imgPath.replace('public/images/','/storage/thumbnails/600/');
                  var t1000 = imgPath.replace('public/images/','/storage/thumbnails/1000/');
                  var tFull = imgPath.replace('public/images/','/storage/images/');

                  $('#figure-container').append(`
              <figure class="grid-item" itemprop="associatedMedia" itemscope="" itemtype="http://schema.org/ImageObject">
                  <a href="#" data-id="`+ v.id +`" class="delete-image" style="text-align: left; left: 6px; top: 5px; position: absolute;color: #404e67;">
                      <i class="fa fa-close"></i>
                  </a>
                <img class="img-thumbnail img-fluid" src="`+ t200 +`" itemprop="thumbnail" height="100%">
                <a href="`+ t800 +`" target="_blank" class="external-open">
                  <i class="fa fa-external-link-square"></i>
                </a>
                <span class="editable">
                  <a class="editable-wrap">
                    <span><b>@lang('gallery.imageTitle')</b></br><input type="text" name="imageTitle[`+ v.id +`]" value="`+v.title+`" /></span>
                    <span><b>@lang('gallery.imageAlt')</b></br><input type="text" name="imageAlt[`+ v.id +`]" value="`+v.alt+`" /></span>
                      <br>
                      <span><b>`+copyToText+`</b></span>
                      <a href="#" class="gallery-clipboard" data-clipboard-text="{{URL::to('/')}}` + t200 + `">200 </a>
                      <a href="#" class="gallery-clipboard" data-clipboard-text="{{URL::to('/')}}` + t400 + `">400 </a>
                      <a href="#" class="gallery-clipboard" data-clipboard-text="{{URL::to('/')}}` + t600 + `">600 </a>
                      <a href="#" class="gallery-clipboard" data-clipboard-text="{{URL::to('/')}}` + t800 + `">800 </a>
                      <a href="#" class="gallery-clipboard" data-clipboard-text="{{URL::to('/')}}` + t1000 + `">1000 </a>
                      <a href="#" class="gallery-clipboard" data-clipboard-text="{{URL::to('/')}}`+tFull+`">ORIGINAL </a>
                  </a>
                </span>
              </figure>

              `);
              });


          }).fail(function (erroErrorr) {

          });
      });

        var url = '{{route('admin.ajaxSearchImage')}}';

        $("#seachImage").select2({
            minimumInputLength: 2,
            allowClear: true,
            ajax: {
                url: url,
                dataType: 'json',
                type: "GET",
                quietMillis: 50,
                data: function (term) {
                    return {
                        term: term
                    };
                },
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                // cache: true
            }
        });

      $('#xlarge').on('hide.bs.modal', function() {
        location.reload();
      });

        function doDelete(id) {

            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            // if(confirm('You really want to delete this record?')) {

            swal({
                title: "{{__('general.Warning!')}}",
                text: "{{__('general.Are you sure you need to delete this item? this change cannot be undone.')}}",
                icon: "warning",
                buttons: {
                    cancel: {
                        text: "{{__('general.Cancel')}}",
                        value: null,
                        visible: !0,
                        className: "",
                        closeModal: !1
                    },
                    confirm: {
                        text: "{{__('general.Yes.Delete')}}",
                        value: !0,
                        visible: !0,
                        className: "",
                        closeModal: !1
                    }
                }
            }).then(e => {
                if (e) {

                    $.ajax({
                        data: {_token: CSRF_TOKEN, img_id: id},
                        method: 'post',
                        url: "{{ route("admin.deleteImage") }}",
                    }).done(function (response) {
                        swal("{{__('general.Success!')}}", "{{__('general.Item deleted!!')}}", "success").then(() => {
                            location.reload();
                        });
                    }).fail(function (erroErrorr) {
                        swal("{{__('general.Error')}}", "{{__('general.Error Occured')}}", "error");
                    });
                } else {
                    swal("{{__('general.Cancelled')}}", "{{__("general.It's safe")}}", "error");
                }
            });
            return false;
        }

        $('select[name^="form_locale"]').on('change', function (e) {
            let slctLanguage = $(this);
            let CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            let imageId = slctLanguage.attr('data-imageId');
            let language = slctLanguage.val();

            $.ajax({
                data: {_token: CSRF_TOKEN, image_id: imageId, language: language},
                method: 'GET',
                url: '{{ route('admin.ajaxEditImage') }}',
            }).done(function (image) {
                if (image.title) {
                    slctLanguage.closest('.editable').find('input[name^="imageTitle"]').val(image.title);
                }

                if (image.alt) {
                    slctLanguage.closest('.editable').find('input[name^="imageAlt"]').val(image.alt);
                }
            }).fail(function (error) {
                console.log(error);
                let messages = error.responseJSON.msg;

                for (let message in messages) {
                    toastr.error(messages[message], 'Error',{ timeOut: 5000  });
                }
            });
        });

      $('#saveImagesChanges').on('click', function (e) {
            e.preventDefault();

            var saveFormBtn = $(this);
            var saveBtnOriginal = saveFormBtn.html();

            saveFormBtn.html('<i class="fa fa-spinner"></i> Processing');

          var imagesData = $(".my-gallery .editable.ready :input").serialize();
          var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

          $.ajax({
              data: {_token: CSRF_TOKEN, data: imagesData},
              method: 'post',
              url: "{{ route("admin.saveImage") }}",
          }).done(function (response) {
              saveFormBtn.html(saveBtnOriginal);
              toastr.success('Page Updated', 'Success', {
                  timeOut: 5000,
                  fadeOut: 1000,
                  progressBar: true,
              });

          }).fail(function (error) {
              saveFormBtn.html(saveBtnOriginal);
              var messages = error.responseJSON.msg;

              for (message in messages) {
                  toastr.error(messages[message], 'Error',{ timeOut: 5000  });
              }
          });
        });

        var clipboard =  new ClipboardJS('.gallery-clipboard');

        clipboard.on('success', function(e) {
           alert("Copied to clipboard");
        });

      $('.delete-image').on('click',function(){
          var id= $(this).data('id');
          doDelete(id);
      });
    });
  </script>
@endsection
